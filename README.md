# 11ty Example Site

An opinionated bare-bones configuration of a static site using [11ty](https://www.11ty.dev/), hosted on [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/index.html).

You can see the site [here](https://feralresearch.gitlab.io/11ty-example-site/).

# How to use

- Clone this repo
- Edit `BASE_HREF` in `.gitlab-ci.yaml` to point to your pages site (You can find this under settings/pages "Your pages are served under")
- Locally, run `yarn dev` to launch a local live-preview webserver
- Edit/add your content inside `src`
- Anything in `src/_assets` will be copied as-is
- Commit and push to publish
