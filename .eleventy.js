module.exports = (eleventyConfig) => {
  eleventyConfig.setUseGitIgnore(false);
  eleventyConfig.addPassthroughCopy({ "src/_assets": "/" });
  return {
    dir: {
      includes: "../_includes",
      layouts: "../_layouts",
      input: "src/content",
      output: "build"
    },
    templateFormats: ["md", "njk", "html"]
  };
};
